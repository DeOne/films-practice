import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import axios from "axios";

import { useParams } from "react-router";

import { Container, Image } from "react-bootstrap";

import { addFavorite, removeFavorite } from "../actions/films";

import { config } from "../config";
import history from "../utils/history";

const Film = () => {
  const { id } = useParams();
  const [film, setFilm] = useState({});

  const dispatch = useDispatch();
  const { favorites } = useSelector((state) => state.films);

  useEffect(() => {
    async function fetch() {
      setFilm({});
      const { data, status } = await axios.get(
        "https://api.themoviedb.org/3/movie/" +
          id +
          "?api_key=" +
          config.API_KEY +
          "&language=" +
          config.API_LANGUAGE
      );

      if (status !== 200 || data.status_message !== undefined) {
        history.push("/");
        return false;
      }
      setFilm(data);
    }
    fetch();
  }, [id]);

  const favorite = (id, title, e) => {
    e.preventDefault();

    dispatch(addFavorite(id, title));
  };

  const unfavorite = (id, e) => {
    e.preventDefault();

    dispatch(removeFavorite(id));
  };

  return (
    <Container fluid>
      <div className="flex">
        <Image
          className="img"
          src={"https://image.tmdb.org/t/p/w1280" + film.poster_path}
          rounded
        />
        <div className="info">
          <p>
            <b>Название:</b> {film.title}
          </p>
          <p>
            <b>Оригинальное название:</b> {film.original_title}
          </p>
          {film.release_date !== undefined && film.release_date !== null && (
            <p>
              <b>Год:</b> {film.release_date.substr(0, 4)}
            </p>
          )}
          {film.release_date !== undefined && film.release_date !== null && (
            <p>
              <b>Жанр:</b>{" "}
              {film.genres.map((value, index) => {
                return (
                  value.name + (index + 1 !== film.genres.length ? ", " : "")
                );
              })}
            </p>
          )}
          <p>
            <b>Описание:</b> {film.overview}
          </p>
          {favorites.filter((value) => value.id !== film.id).length ===
            favorites.length && (
            <p>
              <a onClick={(e) => favorite(film.id, film.title, e)} href="#">
                Добавить в избранное
              </a>
            </p>
          )}
          {favorites.filter((value) => value.id !== film.id).length !==
            favorites.length && (
            <p>
              <a onClick={(e) => unfavorite(film.id, e)} href="#">
                Удалить из избранного
              </a>
            </p>
          )}
        </div>
      </div>
    </Container>
  );
};

export default Film;
