import React, { useState } from "react";

import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

import configureStore from "./store";

import { Router as BrowserRouter, Route, Switch, Link } from "react-router-dom";
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";

import history from "./utils/history";

import Home from "./containers/Home";
import Favorite from "./containers/Favorite";
import Search from "./containers/Search";
import Film from "./containers/Film";
import NotFound from "./pages/NotFound";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

const { persistor, store } = configureStore();

function App() {
  const [query, setQuery] = useState("");

  const search = (e) => {
    e.preventDefault();
    setQuery("");
    history.push("/search/" + query);

    return false;
  };

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter history={history}>
          <Navbar bg="primary" variant="dark">
            <Navbar.Brand as={Link} to="/">
              Фильмы
            </Navbar.Brand>
            <Nav className="mr-auto">
              <Nav.Link as={Link} to="/">
                Главная
              </Nav.Link>
              <Nav.Link as={Link} to="/favorite">
                Избранное
              </Nav.Link>
            </Nav>
            <Form onSubmit={search} inline>
              <FormControl
                type="text"
                placeholder="Что ищем?"
                className="mr-sm-2"
                value={query}
                onChange={(e) => setQuery(e.target.value)}
              />
              <Button onSubmit={search} variant="outline-light">
                Найти
              </Button>
            </Form>
          </Navbar>
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/favorite" component={Favorite} exact />
            <Route path="/search/:q" component={Search} exact />
            <Route path="/film/:id" component={Film} exact />
            <Route path="*" component={NotFound} />
          </Switch>
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
}

export default App;
