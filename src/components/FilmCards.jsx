import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { Card, ListGroup, ListGroupItem, CardColumns } from "react-bootstrap";

import { addFavorite, removeFavorite } from "../actions/films";

const FilmCards = (props) => {
  const { films } = props;

  const dispatch = useDispatch();
  const { favorites } = useSelector((state) => state.films);

  const favorite = (id, title, e) => {
    e.preventDefault();

    dispatch(addFavorite(id, title));
  };

  const unfavorite = (id, e) => {
    e.preventDefault();

    dispatch(removeFavorite(id));
  };

  return (
    <CardColumns>
      {films.map((film, index) => {
        return (
          <Card key={index} style={{ width: 300 }}>
            {film.poster_path !== undefined &&
              film.poster_path !== null &&
              film.poster_path !== "" && (
                <Card.Img
                  variant="top"
                  src={"https://image.tmdb.org/t/p/w1280" + film.poster_path}
                />
              )}
            <Card.Body>
              <Card.Title>{film.title}</Card.Title>
              <Card.Text>{film.overview}</Card.Text>
            </Card.Body>
            <ListGroup className="list-group-flush">
              {film.release_date !== undefined && film.release_date !== "" && (
                <ListGroupItem>
                  Год: {film.release_date.substr(0, 4)}
                </ListGroupItem>
              )}
              <ListGroupItem>Рейтинг: {film.vote_average}</ListGroupItem>
            </ListGroup>
            <Card.Body>
              <Card.Link href={"/film/" + film.id}>Перейти к фильму</Card.Link>
              {favorites.filter((value) => value.id !== film.id).length ===
                favorites.length && (
                <Card.Link
                  onClick={(e) => favorite(film.id, film.title, e)}
                  href="#"
                >
                  Нравится
                </Card.Link>
              )}
              {favorites.filter((value) => value.id !== film.id).length !==
                favorites.length && (
                <Card.Link onClick={(e) => unfavorite(film.id, e)} href="#">
                  Не нравится
                </Card.Link>
              )}
            </Card.Body>
          </Card>
        );
      })}
    </CardColumns>
  );
};

export default FilmCards;
