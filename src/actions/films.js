export const SET_FAVORITE = "SET_FAVORITE";

export const addFavorite = (id, title) => {
  return (dispatch, getState) => {
    const { favorites } = getState().films;

    let isExist = false;
    favorites.forEach((element) => {
      if (element.id === id) isExist = true;
    });

    if (!isExist) {
      favorites.push({ id, title });
      const list = Array.from(new Set(favorites));

      dispatch({ type: SET_FAVORITE, payload: { favorites: list } });
    }
  };
};

export const removeFavorite = (id) => {
  return (dispatch, getState) => {
    const { favorites } = getState().films;

    const list = favorites.filter((value) => value.id !== id);

    dispatch({ type: SET_FAVORITE, payload: { favorites: list } });
  };
};
