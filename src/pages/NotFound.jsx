import React from "react";

import { Container } from "react-bootstrap";

const NotFound = () => {
  return <Container fluid>Страница не найдена</Container>;
};

export default NotFound;
