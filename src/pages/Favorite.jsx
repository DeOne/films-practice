import React from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { Container } from "react-bootstrap";

import { removeFavorite } from "../actions/films";

const Favorite = () => {
  const dispatch = useDispatch();
  const { favorites } = useSelector((state) => state.films);

  const unfavorite = (id, e) => {
    e.preventDefault();

    dispatch(removeFavorite(id));
  };

  if (favorites.length === 0) return <Container fluid>Список пуст</Container>;
  return (
    <Container fluid>
      <ul>
        {favorites.map((film) => (
          <li>
            <Link to={"/film/" + film.id}>{film.title}</Link> (
            <a onClick={(e) => unfavorite(film.id, e)} href="#">
              Удалить из списка
            </a>
            )
          </li>
        ))}
      </ul>
    </Container>
  );
};

export default Favorite;
