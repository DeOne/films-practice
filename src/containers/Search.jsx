import { connect } from "react-redux";

import Search from "../pages/Search";

const mapStateToProps = (state) => ({
  ...state,
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
