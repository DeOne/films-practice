import { SET_FAVORITE } from "../actions/films";

const initialState = {
  favorites: [],
};

export function filmsReducer(state = initialState, action) {
  switch (action.type) {
    case SET_FAVORITE:
      return { ...state, favorites: action.payload.favorites };

    default:
      return state;
  }
}
