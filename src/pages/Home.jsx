import React, { useState, useEffect } from "react";
import axios from "axios";

import { Container, Jumbotron, ButtonGroup, Button } from "react-bootstrap";

import { config } from "./../config";

import FilmCards from "../components/FilmCards";

const Home = () => {
  const [films, setFilms] = useState([]);
  const [page, setPage] = useState(1);

  useEffect(() => {
    async function fetch() {
      setFilms([]);
      const { data } = await axios.get(
        "https://api.themoviedb.org/3/movie/popular?api_key=" +
          config.API_KEY +
          "&language=" +
          config.API_LANGUAGE +
          "&page=" +
          page
      );
      setFilms(data.results);
    }
    fetch();
  }, [page]);

  return (
    <Container fluid>
      <Jumbotron fluid>
        <Container style={{ textAlign: "center" }}>
          <h1>Добро пожаловать</h1>
          <p>На сайте предоставлена последняя информация о новинках кино</p>
        </Container>
      </Jumbotron>
      <center>
        {films !== undefined && films !== null && <FilmCards films={films} />}
      </center>
      <center>
        <ButtonGroup style={{ marginBottom: 25 }}>
          {page > 1 && (
            <React.Fragment>
              <Button onClick={() => setPage(page - 1)}>Назад</Button>
              <Button>{page - 1}</Button>
            </React.Fragment>
          )}
          <Button disabled>{page}</Button>
          {page < 500 && (
            <React.Fragment>
              <Button onClick={() => setPage(page + 1)}>{page + 1}</Button>
              <Button onClick={() => setPage(page + 2)}>{page + 2}</Button>
              <Button onClick={() => setPage(page + 1)}>Вперёд</Button>
            </React.Fragment>
          )}
        </ButtonGroup>
      </center>
    </Container>
  );
};

export default Home;
