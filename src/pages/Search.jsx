import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import axios from "axios";

import { Container } from "react-bootstrap";

import { config } from "./../config";
import FilmCards from "../components/FilmCards";

const Search = () => {
  const params = useParams();
  const [films, setFilms] = useState([]);

  const { q } = params;

  useEffect(() => {
    async function fetch() {
      setFilms([]);
      const { data } = await axios.get(
        "https://api.themoviedb.org/3/search/movie/?api_key=" +
          config.API_KEY +
          "&language=" +
          config.API_LANGUAGE +
          "&query=" +
          q
      );
      setFilms(data.results);
    }
    fetch();
  }, [q]);

  return (
    <Container fluid>
      <center>
        {films !== undefined && films !== null && <FilmCards films={films} />}
      </center>
    </Container>
  );
};

export default Search;
