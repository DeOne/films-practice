import { connect } from "react-redux";
import Favorite from "../pages/Favorite";

const mapStateToProps = (state) => ({
  ...state,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Favorite);
